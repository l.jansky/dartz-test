import 'package:equatable/equatable.dart';
import 'package:test/test.dart';
import 'package:dartz/dartz.dart';
import 'dart:convert';

class CustomError extends Equatable {
  final String message;
  CustomError(this.message);

  @override
  List<Object> get props => [message];
}

class Storage<T> {
  final Map<String, T> _data;

  Storage(this._data);

  Either<CustomError, T> setItem(String key, T item) {
    if (key.length > 2) {
      return Left(CustomError('Key is too long'));
    }

    if (_data[key] == null) {
      _data[key] = item;
      return Right(_data[key]);
    } else {
      return Left(CustomError('Item already exists'));
    }
  }

  Option<T> getItem(key) {
    return _data[key] != null ? Some(_data[key]) : None();
  }
}

final add = (int x) => (int y) => x + y;
String join(String a, String b) => '$a, $b';

String intToString(int input) => input.toString();

final curriedJoin = curry2(join);

void main() {
  Storage<int> storage;
  final add3 = add(3);

  setUp(() async {
    storage = Storage<int>({'a': 5});
  });

  test('Immutability', () {
    final initialList = [5, 2, 3];

    List<int> append(List<int> list, int value) {
      //list.add(value);
      //return list;
      return [...list, value];
    }

    final resultList = append(initialList, 6);

    expect(initialList, [5, 2, 3]);
    expect(resultList, [5, 2, 3, 6]);
  });

  test('Curry join', () {
    final joinTest = curriedJoin('test');
    final result = joinTest('aaa');
    expect(result, 'test, aaa');
  });

  test('Function composition', () {
    final composedFunction = composeF(intToString, add3);

    String composedF(int x) => intToString(add3(x));

    final result = composedFunction(5);
    expect(result, '8');

    expect(composedF(5), '8');
  });

  group('Option', () {
    test('map over some item', () {
      final item = storage.getItem('a');
      final mappedItem = item.map(add3).map((a) => 'Item is $a');

      final displayValue = mappedItem.getOrElse(() => 'Not found');

      expect(displayValue, 'Item is 8');
    });

    test('map over none', () {
      final item = storage.getItem('x');
      final mappedItem = item.map(add3).map((a) => 'Item is $a');

      final displayValue = mappedItem.getOrElse(() => 'Not found');

      expect(displayValue, 'Not found');
    });

    test('applicative', () {
      final someValue4 = Some(4);
      final someValue5 = Some(5);

      final someAdd4 = someValue4.ap(Some(add));
      final result1 = someValue5.ap(someAdd4);

      final val1 = result1.getOrElse(() => null);
      expect(val1, 9);

      final result2 =
          someValue4.flatMap((a4) => someValue5.map((a5) => add(a4)(a5)));
      final val2 = result2.getOrElse(() => null);

      expect(val2, 9);
    });
  });

  group('Either', () {
    test('map over right item', () {
      final newItem = storage.setItem('n', 10);

      final mappedItem = newItem.map(add3).map((a) => 'Item is $a');
      final displayValue = mappedItem.fold((l) => l, (r) => r);

      expect(displayValue, 'Item is 13');
    });

    test('map over left item', () {
      final newItem = storage.setItem('a', 10);

      final mappedItem = newItem.map(add3).map((a) => 'Item is $a');
      final displayValue = mappedItem.fold((l) => l, (r) => r);

      expect(displayValue, CustomError('Item already exists'));
    });

    test('chain', () {
      final stringStorage =
          Storage<String>({'a': '{"t": "test"}', 'b': 'invalid'});

      Either<CustomError, T> optionToEither<T>(Option<T> opt) =>
          opt.fold(() => Left(CustomError('Not found')), right);

      Either<CustomError, Map<String, dynamic>> parseJsonObject(String input) {
        try {
          final res = json.decode(input);
          if (res is Map<String, dynamic>) {
            return Right(res);
          } else {
            return Left(CustomError('Unable to parse'));
          }
        } catch (error) {
          return Left(CustomError('Unable to parse'));
        }
      }

      final opt1 = stringStorage.getItem('a');
      final result1 = optionToEither(opt1).flatMap(parseJsonObject);
      expect(result1.fold((l) => l, (r) => r), {'t': 'test'});

      final opt2 = stringStorage.getItem('b');
      final result2 = optionToEither(opt2).flatMap(parseJsonObject);
      expect(result2.fold((l) => l, (r) => r), CustomError('Unable to parse'));

      final opt3 = stringStorage.getItem('x');
      final result3 = optionToEither(opt3).flatMap(parseJsonObject);
      expect(result3.fold((l) => l, (r) => r), CustomError('Not found'));
    });
  });
}
